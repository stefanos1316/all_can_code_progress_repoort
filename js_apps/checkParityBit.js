function checkParityBit(x) {

	if (x.charAt(0) === '1') {
		if ((x.match(/1/g).length - 1) % 2 == 1) {
			return true;
		} else {
			return false;
		}
	} else {
		if (x.match(/1/g).length % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}
}

module.exports=checkParityBit;
