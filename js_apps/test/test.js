var expect = require('chai').expect;
var addTwoNumbers = require('../addTwoNumbers');
var checkParityBit = require('../checkParityBit');

describe('addTwoNumbers()', function () {
  it('should add two numbers', function () {
  
	  // 1. Arrange
	  var x = 5;
	  var y = 1;
	  var sum1 = x + y;

	  // 2. Act
	  var sum2 = addTwoNumbers(x, y);

	  // 3. Assert
	  expect(sum2).to.be.equal(sum1);
  });
});


describe('checkParityBit()', function () {
  it('should find if parity or not', function (){
  
	  // 1. Arrange
	  var evenBitsTrue = '011011011';
	  var oddBitsTrue = '1000010011';
	  var evenBitsFalse = '100110101';
	  var oddBitsFalse = '0111111111';

	  // 2. Act
	  var resultsEvenTrue = checkParityBit(evenBitsTrue);
	  var resultsOddTrue = checkParityBit(oddBitsTrue);
	  var resultsEvenFalse = checkParityBit(evenBitsFalse);
	  var resultsOddFalse = checkParityBit(oddBitsFalse);

	  // 3. Assert
	  expect(true).to.be.equal(resultsEvenTrue);
	  expect(true).to.be.equal(resultsOddTrue);
	  expect(false).to.be.equal(resultsEvenFalse);
	  expect(false).to.be.equal(resultsOddFalse);

  });
});
