require "base64"
require "jwt"
require "OpenSSL"
ISSUER_ID = "69a6de82-f9a9-47e3-e053-5b8c7c11a4d1"
KEY_ID = "B9UU8QVCB5"
private_key = OpenSSL::PKey.read("./AuthKey_B9UU8QVCB5.p8")
token = JWT.encode(
   {
    iss: ISSUER_ID,
    exp: Time.now.to_i + 20 * 60,
    aud: "appstoreconnect-v1"
   },
   private_key,
   "ES256",
   header_fields={
     kid: KEY_ID }
 )
puts token
