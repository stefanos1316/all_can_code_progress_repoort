# all_can_code_progress_repoort

This is a repository where I house information associated with my progress in AllCanCode Inc. starting from 24th of December 2018.
I host information related to tools, practices, updates, history, or knowledge that I obtained during my time there.

# Directory Tree
```
project
|   README.md  
└───daily_reports
|   |    2018
|   |     └───December
|   |     |       2018-12-*.txt
|   |     |       [    ...    ]
```
