const fs = require('fs');
const jwt = require('jsonwebtoken');
const agent = require('superagent');
const privateKey = fs.readFileSync('./AuthKey_B9UU8QVCB5.p8');
const csrContent = fs.readFileSync('./CertificateSigningRequest.certSigningRequest');
const apiKeyId = "B9UU8QVCB5";
const issuerId = "69a6de82-f9a9-47e3-e053-5b8c7c11a4d1";


let now = Math.round((new Date()).getTime() / 1000);
let nowPlus20 = now + 1999

let payload = {
    "iss": issuerId,
    "exp": nowPlus20,
    "aud": "appstoreconnect-v1"
}

let signOptions = {
    header : {
        "alg": "ES256",
        "kid": apiKeyId,
        "typ": "JWT"
    }
};

let token =  jwt.sign(payload, privateKey, signOptions);

(async () => {
  try {
    console.log(token);
    var res = await agent
		  .get('https://api.appstoreconnect.apple.com/v1/certificates')
      .set('Authorization', 'Bearer ' + token)
      .timeout(20000);
    console.log(res);

  var content = await agent
		  .get('https://api.appstoreconnect.apple.com/v1/certificates/38778GXZS4')
      .set('Authorization', 'Bearer ' + token)
      .timeout(20000);
    
  console.log(content);

  console.log('Show and get certificate');
  var certificate = await agent
    .get('https://api.appstoreconnect.apple.com/v1/certificates/32W597T29F')
    .set('Authorization', 'Bearer ' + token)
    .timeout(20000);


  console.log('Register a bundleid');
  var createBundleid = await agent
      .post('https://api.appstoreconnect.apple.com/v1/bundleIds')
      .set('Authorization', 'Bearer ' + token)
      .send({
        data: {
          attributes: {
            identifier: "com.allcancode.projectName",
            name: "Just to test App Connect API",
            platform: "IOS",
          },
          type: 'bundleIds'
        }
      })
      .timeout(20000);

    var bundleID = createBundleid.body.data.id;
    var modifyBundleCapability = await agent
      .post('https://api.appstoreconnect.apple.com/v1/bundleIdCapabilities')
      .set('Authorization', 'Bearer ' + token)
      .send({
        data: {
          attributes: {
            capabilityType: 'ASSOCIATED_DOMAINS'
          },
          relationships: {
            bundleId: {
              data: {
                id: bundleID,
                type: 'bundleIds'
              }
            }
          },
          type: 'bundleIdCapabilities'
        }
      })
      .timeout(20000);

  var getDeviceID = await agent
      .get('https://api.appstoreconnect.apple.com/v1/devices')
      .set('Authorization', 'Bearer ' + token)
      .timeout(20000);

  console.log('Creating profile');
  var createProfile = await agent
      .post('https://api.appstoreconnect.apple.com/v1/profiles')
      .set('Authorization', 'Bearer ' + token)
      .send({
        data: {
          attributes: {
            name: 'Just_a_new_profile',
            profileType: 'IOS_APP_DEVELOPMENT'
          },
          relationships: {
            bundleId: {
              data: {
                id: bundleID,
                type: 'bundleIds'
              }
            },
            certificates: {
              data: [{
                id: '38778GXZS4',
                type: 'cerfiticates'
              }]
            },
            devices: {
              data: [{
                id: 'B39RNV2X65',
                type: 'devices'
              }]
            }
          },
          type: 'profiles'
        }
      })
      .timeout(20000);

  console.log('Profile created');


  var deleteBundleid = await agent
    .delete('https://api.appstoreconnect.apple.com/v1/bundleIds/' + bundleID)
    .set('Authorization', 'Bearer ' + token)
    .timeout(20000);
    
  console.log('BundleID deleted');


  console.log('Creating certificate');
  var certificateCreate = await agent
		  .post('https://api.appstoreconnect.apple.com/v1/certificates')
      .set('Authorization', 'Bearer ' + token)
      .send({
        data: {
          attributes: {
            certificateType: 'IOS_DEVELOPMENT',
            csrContent: csrContent
          },
          type: 'certificates'
        }
      })
      .timeout(20000);

  //console.log(certificateCreate);
  console.log('Creating mobile provisioning');

  console.log('End of test');

  } catch (err) {
    console.error(err);
  }
})();

